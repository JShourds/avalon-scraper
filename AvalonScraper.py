from lxml import html
import re
import requests
import schedule
import time
from datetime import datetime
import pymysql


def getData():
    ipList = ['10.10.67.254', '10.10.68.254', '10.10.68.2', '10.10.68.3', '10.10.68.4', '10.10.68.5', '10.10.68.6',
              '10.10.68.7', '10.10.68.8', '10.10.68.9', '10.10.68.10']

    fileClear = open(r'trimmedLog.txt', 'w')
    fileClear.close()

    currentTime = datetime.now()
    timestamp = currentTime.strftime("%Y-%m-%d %H:%M:%S")
    print("Fetching Avalon data at " + str(timestamp))

    try:
        conn = pymysql.connect(host='10.20.0.220', port=3306, user='Justin',
                               password='Password', db='Tracker')
        cursor = conn.cursor()

    except Exception as e:
        print("Connection to database failed - " + str(e))
        print('Tables not updated...')
        return

    # UNCOMMENT TO DELETE TABLE BEFORE SCANNING (USE ONLY WHEN REMOVING/ADDING AVALONS) #
    # Uncomment lines 141-144 as well, as this code inserts new data into the table after its cleared #
    # try:
    #     clearTable = """DELETE FROM `Avalons_RealTime`"""
    #     cursor.execute(clearTable)
    #
    # except Exception as e:
    #     print('FAILED to clean Avalons_RealTime - ' + str(e))

    conn.commit()

    num = 1
    poolAcc = {}
    pool = ''
    acc = ''
    templine = ''
    poolInfo = {}


    for pi in ipList:
        poolTemp = ''
        accTemp = ''

        file3 = open(r'avalontest.txt', 'w')
        REQUEST_URL = 'http://'+pi+'/cgi-bin/luci/admin/status/cgminerstatus/'

        payload = {'luci_username': 'root', 'luci_password': 'root'}

        with requests.Session() as session:

            try:
                post = session.post(REQUEST_URL, data=payload, timeout=2)
                file3.write(post.text)
                file3.close()
                file3 = open('avalontest.txt', 'r')

                # Find Pool 1 info for Avalons #
                pool1 = ''
                linecount = 0
                for line in file3:
                    linecount += 1
                    if 'cbi-table-1-url' in line:
                        linecount = linecount + 2
                        pool1temp = file3.readlines(linecount)
                        pool1 = pool1temp[2]
                        poolInfo['Pool 1'] = pool1.strip()
                        # print(pool1)
                        break

                file3.close()

                file3 = open('avalontest.txt','r')

                # Find Pool 1 STATUS #
                pool1status = ''
                linecount = 0
                for line in file3:
                    linecount += 1
                    if 'cbi-table-1-stratumactive' in line:
                        linecount = linecount + 2
                        pool1statustemp = file3.readlines(linecount)
                        pool1status = pool1statustemp[2]
                        poolInfo['Pool 1 Status'] = pool1status.strip()
                        # print(pool1status)
                        break

                file3.close()

                file3 = open('avalontest.txt','r')

                # Find Pool 2 info for Avalons #
                pool2 = ''
                linecount = 0
                for line in file3:
                    linecount += 1
                    if 'cbi-table-2-url' in line:
                        linecount = linecount + 2
                        pool2temp = file3.readlines(linecount)
                        pool2 = pool2temp[2]
                        poolInfo['Pool 2'] = pool2.strip()
                        break

                file3.close()

                file3 = open('avalontest.txt','r')

                # Find Pool 2 STATUS #
                pool2status = ''
                linecount = 0
                for line in file3:
                    if 'cbi-table-2-stratumactive' in line:
                        linecount = linecount + 2
                        pool2statustemp = file3.readlines(linecount)
                        pool2status = pool2statustemp[2]
                        poolInfo['Pool 2 Status'] = pool2status.strip()
                        break

                file3.close()

                file3 = open('avalontest.txt', 'r')
                # print(poolInfo)
            except Exception as e:
                pass

            try:
                with requests.Session() as session:
                    file = open('avalonAcc.txt','w')
                    REQUEST_URL = 'http://' + pi + '/cgi-bin/luci/admin/status/cgminer/'

                    payload = {'luci_username': 'root', 'luci_password': 'root'}
                    post = session.post(REQUEST_URL, data=payload, timeout=2)
                    file.write(post.text)
                    file.close()
                    file = open('avalonAcc.txt', 'r')

                    # Find account info for Avalons #
                    for line in file:
                        if 'pool1user' in line:
                            templine = line
                            value = templine.find('value=')
                            if value != (-1):
                                acc = line[(value + 7):-5]
                                break

                    acc = acc.split('.')[0]
            except Exception as e:
                pass

    for pi in ipList:
        file = open(r'logDump.txt', 'w')
        REQUEST_URL = 'http://'+pi+'/cgi-bin/luci/admin/status/cgminerapi/'

        payload = {'luci_username': 'root', 'luci_password': 'root'}

        try:
            with requests.Session() as session:
                post = session.post(REQUEST_URL, data=payload, timeout=2)
                file.write(post.text)
                file.close()
                file = open(r'logDump.txt', 'r')
                lines = file.readlines()
                file2 = open(r'trimmedLog.txt', 'a')
                file2.write(pi + '\n')
                file2.write('--------------------\n')
                dataList = ['DNA', 'Elapsed', 'MW', 'HW', 'LW', 'MH', 'HW', 'DH', 'Temp', 'TMax',
                            'Fan', 'FanR', 'Vi', 'Vo', 'GHSmm', 'WU', 'Freq', 'PG', 'Led', 'MW0', 'MW1', 'MW2', 'MW3', 'TA',
                            'ECHU', 'ECMM', 'FM']

                dnaList = []
                dataDict = {}
                check = lines
                row = -1
                end = -1
                count = 0
                for z in check:

                    # 'STATS0 indicates the end of the set of data we are collecting #
                    if 'STATS0' in z:
                        dnas = open('dnas.txt', 'w')

                        # Converting list to a dict, then back to a list, in order to remove duplicates #
                        dnaList = list(dict.fromkeys(dnaList))
                        break

                    if 'DNA' in z:

                        # Getting DNA value from API log & stripping extra characters off #
                        dna = z.split('[')[1]
                        dna = dna.split(']')[0]

                        row += 1
                        count += 1
                        file2.write('Machine ' + str(count) + '\n')
                        dnaList.append(z)
                        check = lines

                        for y in check:

                            if 'FM' in y:
                                end += 1

                                for i in check:
                                    if 'CRC' in i:
                                        break

                                    for x in dataList:
                                        if x in i:
                                            waste = i.split('[')[0]
                                            waste2 = waste.split(' ')[4]

                                            if waste2 == x:
                                                x = str(x)
                                                res = i.partition('[')[2]
                                                res = res[:-2]
                                                dataDict[x] = str(res)
                                                check = lines[row:end]
                                                break

                                # file2.write(dataDict['DNA'] + '\n')
                                file2.write('DNA: ' + dataDict['DNA'] + '\n'
                                            + 'Seconds elapsed: ' + dataDict['Elapsed'] + '\n'
                                            + 'GH/s: ' + dataDict['GHSmm'] + '\n'
                                            + 'Fan RPM: ' + dataDict['Fan'] + '\n' + '\n ' + '\n')

                                # Assigning variables with values to display in table #
                                pool1 = poolInfo['Pool 1']
                                pool1status = poolInfo['Pool 1 Status']
                                pool2 = poolInfo['Pool 2']
                                pool2status = poolInfo['Pool 2 Status']
                                elapsed = dataDict['Elapsed']
                                hashrate = dataDict['GHSmm']  # Average hashrate (GH/s)
                                dna = dataDict['DNA']  # DNA of machine
                                fanSpeed = dataDict['Fan']  # Fan speed in RPM
                                fanR = dataDict['FanR']  # Control value for fan speed
                                tempIn = dataDict['Temp']  # Intake temperature
                                tempMax = dataDict['TMax']  # Temp of hottest ASIC chip
                                Freq = dataDict['Freq']  # Average frequency
                                PG = dataDict['PG']  # Power Good code (15 is good)
                                TA = dataDict['TA']  # Total available chips (104 is good)
                                dna2 = dna  # Another DNA variable used as a reference when updating the table

                                # Update Avalons_RealTime table
                                try:
                                    executeStr = """UPDATE `Avalons_RealTime`
                                                    SET IP = %s, DNA = %s, Elapsed = %s, Hashrate = %s, fanSpeed = %s,
                                                     fanR = %s, tempIn = %s, tempMax = %s, Freq = %s, PG = %s, TA = %s,
                                                     date_time = %s, Account = %s, Pool = %s, Pool_Status = %s, Pool2 = %s,
                                                     Pool2_Status = %s
                                                    WHERE DNA = %s"""
                                    cursor.execute(executeStr, (pi, dna, elapsed, hashrate, fanSpeed, fanR, tempIn, tempMax,
                                                                Freq, PG, TA, timestamp, acc, pool1, pool1status,
                                                                pool2, pool2status, dna2))

                                    # UNCOMMENT TO ADD TO TABLE RATHER THAN UPDATE (ONLY USE WHEN REMOVING/ADDING AVALONS) #
                                    # Uncomment lines 29-38 as well, as that code clears the RealTime table #
                                    # executeStr = """INSERT INTO `Avalons_RealTime` (IP, DNA, Elapsed, Hashrate, fanSpeed,
                                    # fanR, tempIn, tempMax, Freq, PG, TA, date_time, Account, Pool, Pool_Status, Pool2,
                                    # Pool2_Status)
                                    #                 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
                                    # cursor.execute(executeStr, (pi, dna, elapsed, hashrate, fanSpeed, fanR, tempIn, tempMax,
                                    #                             Freq, PG, TA, timestamp, acc, pool1, pool1status, pool2,
                                    #                             pool2status))

                                except Exception as e:
                                    print("FAILED updating Avalons_RealTime - " + str(e))

                                conn.commit()
                                num += 1
                                check = lines[row:end]
                                break

                            else:
                                end += 1
                            check = lines[row:end]

                    else:
                        row += 1
        except Exception as e:
            pass

    # Appending Avalons_RealTime table to Avalons_Historical table as RealTime is updated #
    if ':00:00' in timestamp:
        print("saving to 'Avalons_Historical'")
        try:
            saveHistory = """INSERT INTO `Avalons_Historical` (IP, DNA, Elapsed, Hashrate, fanSpeed, fanR, tempIn, tempMax, Freq, PG, TA, date_time, Account, Pool, Pool_Status, Pool2, Pool2_Status)
            SELECT IP, DNA, Elapsed, Hashrate, fanSpeed, fanR, tempIn, tempMax, Freq, PG, TA, date_time, Account, Pool, Pool_Status, Pool2, Pool2_Status FROM `Avalons_RealTime`"""
            cursor.execute(saveHistory)
        except Exception as e:
            print("FAILED to save history - " + str(e))
    else:
        pass

    conn.commit()
    file.close()
    file2.close()
    file3.close()
    cursor.close()
    conn.close()


schedule.every().minute.at(":00").do(getData)

while True:
    schedule.run_pending()
    time.sleep(1)
