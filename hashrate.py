import json
import time
import hmac
import hashlib
import requests
import csv
import pymysql
import schedule
from datetime import datetime

####################################
#    AntPool.com API Parameters    #
####################################

# AntPool API Parameters #
coin_type='BTC'
sign_id = 'psbackup'
sign_key = '94747eae2fa743b1b0155c2ed12234ea'
sign_SECRET = '1536b79519674121ad8af82a2a5ba082'

# Webpages for AntPool API #
html_poolstats = 'https://antpool.com/api/poolStats.htm'  # Not used - info isn't useful
html_balance = 'https://antpool.com/api/account.htm'  # Not used
html_hashrate_user = 'https://antpool.com/api/hashrate.htm'  # Reports account hashrate
html_hashrate_miner = 'https://antpool.com/api/workers.htm'  # Reports hashrates of miners on account
html_payment = 'https://antpool.com/api/paymentHistory.htm'  # Not used

####################################
#    Bitcoin.com API Parameters    #
####################################

# Bitcoin.com API keys #
apiKeys = {}
apiKeys["projspok"] = "bb05ee19-eb75-495a-b76f-43dd4cf49cce"
apiKeys["projspok2"] = "aa2b63e2-491f-401b-ae76-6eb9f8dbc075"
apiKeys["projspok3"] = "38e5a65a-8cf8-4df9-8173-d51f5e4a9c35"
apiKeys["projspok5"] = "257a24e5-e087-4ed8-9b2e-5049f85640a9"
apiKeys["hyperb"] = "022aee88-839a-4129-8036-f24d5e8f2708"
apiKeys["HBtest"] = "e7e0e395-ba18-4c70-ab46-c0a93b89f73b"


# Create signature for authorization #
def get_signature():
    try:
        nonce = int(time.time() * 1000)
        msgs = sign_id + sign_key + str(nonce)
        ret = []
        ret.append(hmac.new(sign_SECRET.encode(encoding="utf-8"), msg=msgs.encode(encoding="utf-8"),
                            digestmod=hashlib.sha256).hexdigest().upper())
        ret.append(nonce)
        return ret

    except Exception as e:
        print('Failed to get signature for Antpool API - ' + str(e))
        pass

# Get hashrate info for account #
def get_messages_pool(url):
    file = open('psbackupStats.txt', 'w')
    api_sign = get_signature()
    post_data = {'key': sign_key, 'nonce': api_sign[1], 'signature': api_sign[0], 'coin': coin_type}

    try:
        request = requests.post(url, data=post_data)
        file.write(request.text)
        file.close()
        file = open('psbackupStats.txt', 'r')

        data = ''
        for line in file:
            try:
                data = json.loads(line)
            except Exception as e:
                print('Unable to get JSON data - ' + str(e))
                return
    except Exception as e:
        print('Failed to get JSON data from Antpool - ' + str(e))

# Get page number for worker list dynamically #
def getPages(url):

    file = open('psbackupMiners.txt', 'w')

    try:
        api_sign = get_signature()
        post_data = {'key': sign_key, 'nonce': api_sign[1], 'signature': api_sign[0], 'coin': coin_type, 'page': 1,
                     'pageSize': 500}

        request = requests.post(url, data=post_data)
        file.write(request.text)
        file.close()
        file = open('psbackupMiners.txt', 'r')

        data = []
        for line in file:
            data.append(json.loads(line))

        # print(data[0]['data']['totalPage'])
        pages = int(data[0]['data']['totalPage'])
        get_messages_miner(url, pages)

    except Exception as e:
        print('Failed to fetch number of pages for Antpool data: ' + str(e))


# Fetch data for each machine listed in 'Workers' page #
def get_messages_miner(url, pages):

    # ANTPOOL MINERS #
    print('Collecting ' + str(pages) + ' pages of Antpool.com miner data...')
    minersCsv = open('psbackupMiners.csv', 'w', newline='')
    csvwriter = csv.writer(minersCsv)
    bigList = []
    jsonList = []
    file = open('psbackupMiners.txt', 'w')
    api_sign = get_signature()

    try:
        for i in range(pages):
            post_data = {'key': sign_key, 'nonce': api_sign[1], 'signature': api_sign[0],
                         'coin': coin_type, 'page': i, 'pageSize': 500}

            request = requests.post(url, data=post_data)
            file.write(request.text + '\n')

        file.close()
    except Exception as e:
        print('Failed to get Antpool.com data: ' + str(e))

    try:

        file = open('psbackupMiners.txt', 'r')

        for x in file:
            bigList.append(x)
        file.close()
        file = open('biglist.txt', 'w')

        for data in bigList:
            file.write(data)
            jsonList.append(json.loads(data))
        csvwriter.writerow(['Account', 'Worker', 'hashrateNow', 'hashrate1h'])

        workerData = []
        workers = []

    except Exception as e:
        print('Failed to create bigList.txt' + str(e))

    try:
        # x corresponds to page number (Total worker entries / Entries per page) #
        for x in range(len(jsonList)):

            # i corresponds to miner number on page (80 miners per page) #
            for i in range(len(jsonList[0]['data']['rows'])):

                # Checking if current worker has already been added to list of workers #
                if jsonList[x]['data']['rows'][i]['worker'] in workers:
                    break

                # Adding account name to a CSV spreadsheet #
                workerData.append(sign_id)  # Writing account name to table

                try:
                    # Adding worker name to list for preventing duplicate entries #
                    workers.append(jsonList[x]['data']['rows'][i]['worker'])

                    # Writing worker data to a row in the CSV spreadsheet #

                    # Some workernames are messed up, and are the same as the account name, meaning theres nothing to split #
                    # This checks the length of the worker name and determines if it can be split or not #
                    if len(jsonList[x]['data']['rows'][i]['worker']) > 9:

                        workerData.append(jsonList[x]['data']['rows'][i]['worker'].split('.')[1])  # Worker Name
                        workerData.append(str(float(jsonList[x]['data']['rows'][i]['last10m']) / 1000000))
                        workerData.append(str(float(jsonList[x]['data']['rows'][i]['last1h']) / 1000000))
                        csvwriter.writerow(workerData)

                    # Worker name is too short and cannot be split #
                    else:

                        workerData.append(jsonList[x]['data']['rows'][i]['worker'])
                        workerData.append(str(float(jsonList[x]['data']['rows'][i]['last10m']) / 1000000))
                        workerData.append(str(float(jsonList[x]['data']['rows'][i]['last1h']) / 1000000))
                        csvwriter.writerow(workerData)

                except Exception as e:
                    print('Failed to add ' + str(jsonList[x]['data']['rows'][i]['worker']) + ' to spreadsheet - ' + str(e))

                # Clearing list of worker data for the next worker #
                workerData = []

    except Exception as e:
        print('Failed to create CSV, program may require start: ' + str(e))
        return

    # BITCOIN.COM MINERS #

    # Creating list of account names for iterating #
    try:
        print('Collecting Bitcoin.com miner data...')
        keyList = apiKeys.keys()
        workerData = []

        for key in keyList:
            try:
                url = 'https://console.pool.bitcoin.com/srv/api/workers?apikey=' + apiKeys[key]
                request = requests.post(url)
                rawWorkers = json.loads(request.text)
                if len(rawWorkers) > 0:

                    for i in range(len(rawWorkers)):
                        workerData.append(key)

                        try:
                            workerData.append(rawWorkers[i]['workername'].split('.')[1])
                            workerData.append(rawWorkers[i]['hashrateNowTerahashes'])
                            workerData.append(rawWorkers[i]['hashrate1hrAverageTerahashes'])
                            csvwriter.writerow(workerData)

                        except Exception as e:
                            print('Failed to append CSV - ' + str(e))
                        workerData = []

            except Exception as e:
                print('Failed to collect Bitcoin.com data: ' + str(e))

        minersCsv.close()

    except Exception as e:
        print('Failed to collect and store Bitcoin.com data - ' + str(e))

# Update `miners` SQL table #
def updateSQL(timestamp):

    print("Updating 'minersHistory' SQL Table...")
    file = csv.reader(open('psbackupMiners.csv', 'r'))
    next(file, None)

    worker = ''
    account = ''
    old_account = ''
    skipped = 0
    count = 0
    # Iterating through CSV file containing the workers #
    try:
        for row in file:

            account = row[0]
            worker = row[1]

            if account != old_account:
                print(account)
                old_account = account

            if float(row[2]) != 0.0:
                try:
                    conn = pymysql.connect(host='10.20.0.220', port=3306, user='Justin',
                                           password='Password', db='Tracker')
                    cursor = conn.cursor()

                except Exception as e:
                    print("Connection to database failed - " + str(e))
                    return

                try:
                    saveHistory = """INSERT INTO `minersHistory` (Account, Worker, hashrateNow, hashrate1h, date_time)
                                    VALUES (%s, %s, %s, %s, %s)"""
                    cursor.execute(saveHistory, (row[0], row[1], row[2], row[3], timestamp))

                    # updateStr = """UPDATE `miners`
                    #                 SET Account = %s, Worker = %s, hashrateNow = %s, hashrate1h = %s, date_time = %s
                    #                 WHERE Worker = %s"""
                    # cursor.execute(updateStr, (row[0], row[1], row[2], row[3], timestamp, worker))

                    conn.commit()
                    count += 1

                except Exception as e:
                    print('Failed to update SQL table - ' + str(e))

            elif float(row[2]) == 0.0:
                skipped += 1

        print("'minersHistory' updated!")
        print(str(count) + ' machines added to SQL table')
        print(str(skipped) + ' machines skipped w/ no reported hashrate\n\n')

    except Exception as e:
        print('Failed to update SQL - ' + str(e))

# Main #
def main():
    print('hashrate.py starting...')
    currentTime = datetime.now()
    timestamp = currentTime.strftime("%Y-%m-%d %H:%M:%S")
    print(timestamp)
    get_messages_pool(html_hashrate_user)
    getPages(html_hashrate_miner)
    updateSQL(timestamp)
    return


# main()

schedule.every().hour.at(":00").do(main)

while True:
    schedule.run_pending()
    time.sleep(1)
